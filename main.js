


import Printer from './printer';
import ConsoleOutput from './console-output';


//const consoleOutput = ConsoleOutput();
//const printer = Printer(consoleOutput);

const minimist = require('minimist');
const meow = require('meow');
const config = require('./config.json');
const Scraper = require('./scraper');




const cli = meow(`
    Usage
      $ search <"search term(s)"> [options]
 
    Options
      --engine=<engine>, -e=<engine>  Search engine to use (defaults to google)
 
    Other options:
        -h, --help         show usage information
        -v, --version      print version info and exit 
 
    Examples
      $ search "to be or not to be" -e=google
`, {
	flags: {
		engine: {
			type: 'string',
			default: 'google',
			alias: 'e'
		},
		version: {
			type: 'boolean',
			default: false,
			alias: 'v'
		},

	}
});


// create a screenshot dir if not exists
var fs = require('fs');
var dir = './screenshots';

if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}



function main(search_string, flags){
    
    //console.log(search_string);
    //console.log(flags.e);
    

    const search_engine = flags.e;     
    
    
    if(!search_string){
        console.log("\nError, no search string specified!\n");        
        process.exit(1);
    }
    
    
    if(!search_engine){   // this can only happen if default is not properly set for engine
        process.exit(1);
    }
    
    // check config if this is a known engine
    if( !config[search_engine] ){
        console.log("\nError, unsupported search engine specified: '" + search_engine + "'\nSupported engines are:");
        
        Object.entries(config).forEach(([key, value]) => {
            console.log("\t- "+key);
        });
        console.log("");
            
        process.exit(2);
    }

    const scraper = new Scraper(config, Printer(ConsoleOutput()));

    scraper.run(search_string, search_engine);


}


main(cli.input[0], cli.flags);
