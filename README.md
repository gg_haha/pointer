### What is this repository for? ###

Project code for performing commandline searching with online websearch engines by scraping result pages using headless puppeteer.



### Dependencies
* Docker Compose for deploying the project.



### How to setup:

This project uses docker-compose, make sure you have this installed.

Next, cd into project and execute: 

```
. ./init install
```


### Configuration
    
Add selectors for other search engines in config.json



## How clean up:
in terminal run: 

```
. ./init uninstall
```


### How to use


After running install an alias is created so instead of running:

```
docker-compose exec app node search <searchstring> [-e searchengine]
```

you can just run:

```
search <searchstring> [-e searchengine]
```

For aditional options run: 

```
search --help
```

On Linux you can click the resulting url's with ctrl+left-click to open in you default browser.

To verify the results you can check the screenshot in screenshots directory.
The screenshot is updated for every search instance.

