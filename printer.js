export default (output) => {
    return {
        print(data) {

            output.send("\n############################\nShowing the first " + data.length + " results:\n----------------------------\n" );
            
            for (let item of data) {
                output.send(item.title);
                output.send(item.url);
                output.send('');
            }
            
            output.send("############################\n");

        }
    };
}
