const puppeteer = require('puppeteer');



class Scraper {

    constructor(config, printer) { 
        this.config = config;
        this.printer = printer;
        this.results = [];
    }
    
    async run(search_string, search_engine) {

        if(!search_string){
            console.log("\nError, no search string specified!\n");        
            process.exit(2);
        }
        
        if(!search_engine){   // this can only happen if default is not properly set for engine
            console.log("\nError, no search engine specified!\n");              
            process.exit(2);
        }        
            
        const browser = await puppeteer.launch(); 
        //const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});  // @see: todo
        
        const page = await browser.newPage();
        page.on('console', consoleObj => console.log(consoleObj.text()));

        await page.goto(this.config[search_engine].url);
        
        // get selectors from config
        const SEL_SEARCH_FIELD =  this.config[search_engine].sel_search_field;
        const SEL_SEARCH_BUTTON = this.config[search_engine].sel_search_button; 
        const RES_ELEM_CLASSNAME = this.config[search_engine].result_element_class_name;

        // selector function for result title
        const res_title = function (index = 1) {
            return "#rso > div > div > div:nth-child(" + index + ") > div > div > div.r > a > h3";
        }

        // selector function for result url
        const res_url = function (index = 1) {
            return "#rso > div > div > div:nth-child(" + index + ") > div > div > div.r > a > div > cite";
        }

        //await page.click(config.google.sel_search_field);
        await page.evaluate((selector) => {
                document.querySelectorAll("div[style='display:none']").forEach(e => e.parentNode.removeChild(e));   // clearout all none-display elements so we dont click a non display element (which are there)
                document.querySelector(selector).click();
            }, SEL_SEARCH_FIELD );    
        
        
        await page.keyboard.type(search_string);                                       // enter search term(s)
        
        await page.evaluate((selector) => {
                document.querySelectorAll("div[style='display:none']").forEach(e => e.parentNode.removeChild(e));
                document.querySelector(selector).click();
            }, SEL_SEARCH_BUTTON );        
        
        await page.waitFor(2 * 1000);                                               // wait long enough for a result to emerge
        await page.screenshot({path: 'screenshots/output.png', clip:{x:0, y:0, width:800, height:1800} });                    // make screenshot, could be used for debugging

        // lets count number of elements containing results
        let listLength = await page.evaluate((sel) => {
            return document.getElementsByClassName(sel).length;
        }, RES_ELEM_CLASSNAME);

        // loop over all results based on number of results 
        // TODO: how about we implement with a 'while result do' since num res not always seems reliable
        for (let i = 1; i <= listLength; i++) {

            let title = await page.evaluate((sel) => {
                let element = document.querySelector(sel);
                return element ? element.innerHTML : null;
            }, res_title(i));

            let url = await page.evaluate((sel) => {
                let element = document.querySelector(sel);
                return element ? element.innerHTML : null;
            }, res_url(i));

            let result = {};
            
            result.title = title;
            result.url = url;

            if(title && url){              
                
                this.results.push(result);

            }
        }

        this.results.sort(function(a, b){
            if(a.title.toLowerCase() < b.title.toLowerCase()) { return -1; }
            if(a.title.toLowerCase() > b.title.toLowerCase()) { return 1; }
            return 0;
        })
            
        this.printer.print(this.results);

        browser.close();
        
        

        
    }
    
    
    
    

}

module.exports = Scraper;


